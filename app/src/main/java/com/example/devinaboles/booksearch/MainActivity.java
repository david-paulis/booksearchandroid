package com.example.devinaboles.booksearch;

import android.content.Intent;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import java.util.List;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);


        final Button search = (Button) findViewById(R.id.search_btn);
        search.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                EditText editText = (EditText) findViewById(R.id.search_key);
                String searchKey = editText.getText().toString();
                if (!searchKey.isEmpty()) {
                    Intent intent = new Intent(getApplicationContext(), Result.class);
                    String Url="http://content.guardianapis.com/search?q=debates&api-key=test";
                    //String url = "https://www.googleapis.com/books/v1/volumes?q=" + searchKey + "&maxResults=15";
                    intent.putExtra("url", Url);
                    startActivity(intent);
                } else {
                    Toast.makeText(MainActivity.this, "Please enter a word", Toast.LENGTH_SHORT).show();
                }
            }
        });
    }

}
