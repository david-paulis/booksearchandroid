package com.example.devinaboles.booksearch;

import android.content.AsyncTaskLoader;
import android.content.Context;

import org.json.JSONException;

import java.util.List;

/**
 * Created by Devina Boles on 08/07/2017.
 */
public class BookLoader extends AsyncTaskLoader<List<Book>> {
    private String mUrl;

    public BookLoader(Context context, String url) {
        super(context);
        mUrl = url;
    }

    @Override
    protected void onStartLoading() {
        forceLoad();
    }

    @Override
    public List<Book> loadInBackground() {
        List<Book> books = null;
        try {
            books = QueryUtils.fetchBookData(mUrl);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return books;
    }
}
