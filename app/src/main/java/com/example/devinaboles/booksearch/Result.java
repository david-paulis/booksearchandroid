package com.example.devinaboles.booksearch;

import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import android.app.LoaderManager;
import android.app.LoaderManager.LoaderCallbacks;
import android.content.Loader;
import android.widget.Toast;

import static android.content.Intent.ACTION_VIEW;

public class Result extends AppCompatActivity implements LoaderCallbacks<List<Book>> {

    private BookAdapter madapter;
    private String mUrl;
    TextView mEmptyState;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_result);

        madapter = new BookAdapter(this, new ArrayList<Book>());

        ListView listView = (ListView) findViewById(R.id.list);
        listView.setAdapter(madapter);
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                   Book book=madapter.getItem(position);
                   String Url = book.getUrl();
                Intent intent=new Intent(ACTION_VIEW, Uri.parse(Url));
                startActivity(intent);

            }
        });

        mEmptyState = (TextView) findViewById(R.id.empty_view);
        listView.setEmptyView(mEmptyState);
        Bundle extras = getIntent().getExtras();
        mUrl = extras.getString("url");
        mUrl = mUrl.replace(" ", "%20");
        ConnectivityManager cm =
                (ConnectivityManager) this.getSystemService(this.CONNECTIVITY_SERVICE);

        NetworkInfo activeNetwork = cm.getActiveNetworkInfo();
        boolean isConnected = activeNetwork != null && activeNetwork.isConnectedOrConnecting();

        if (isConnected) {
            LoaderManager loaderManager = getLoaderManager();
            loaderManager.initLoader(1, null, this);
        } else {
            View loadingIndicator = findViewById(R.id.loading_indicator);
            loadingIndicator.setVisibility(View.GONE);
            TextView textView = (TextView) findViewById(R.id.no_internet);
            textView.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public Loader<List<Book>> onCreateLoader(int id, Bundle args) {
        return new BookLoader(this, mUrl);
    }

    @Override
    public void onLoadFinished(Loader<List<Book>> loader, List<Book> data) {
        mEmptyState.setText(R.string.empty_state);
        View loadingIndicator = findViewById(R.id.loading_indicator);
        loadingIndicator.setVisibility(View.GONE);
        madapter.clear();
        if (data != null) {
            madapter.addAll(data);
        }

    }

    @Override
    public void onLoaderReset(Loader<List<Book>> loader) {
        madapter.clear();

    }


}
