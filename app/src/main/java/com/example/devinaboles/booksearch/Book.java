package com.example.devinaboles.booksearch;

/**
 * Created by Devina Boles on 08/07/2017.
 */

public class Book {
    private String title;
    private String author;
    private String url;

    public String getUrl() {
        return url;
    }

    public String getTitle() {
        return title;
    }

    public String getAuthor() {
        return author;
    }

    public Book(String title, String author,String url) {
        this.url=url;
        this.title = title;
        this.author = author;
    }
}
